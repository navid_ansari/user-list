registerModule.controller('RegisterCtrl', ['$scope','$state', 'RegisterService', '$cordovaCamera', function ($scope,$state, RegisterService, $cordovaCamera) { //$cordovaCamera

    console.log("register controller loaded");

    var vm = this;
    vm.username_regex = USERNAME_REGEX;
    vm.email_regex = EMAIL_REGEX;
    vm.profilePic = "";
    vm.imgThumb = false;
    vm.PictureBtn = 'Click Picture';
    this.clickPic = function () {

        var options = {
            quality: 30,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            //encodingType: Camera.EncodingType.JPEG,
            //targetWidth: 100,
            //targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            //saveToPhotoAlbum: false,
            //correctOrientation: true
        };

        $cordovaCamera.getPicture(options).then(

            function (imageData) {
                console.log("image data:" + angular.toJson(imageData));
                //console.log("image data in stringify:" + JSON.stringify(imageData));
                vm.imgThumb = true;
                vm.PictureBtn = 'Change Picture';
                vm.profilePic = imageData;
            },
            function (err) {
                console.log("image data error:" + err);
            })

    };

    this.checkEmailExist = function (email) {

        console.log("check email: " + JSON.stringify(email));
        console.log(email.email);

        LoginService.checkEmailExist(email.email).then(

            function (success) {

            },
            function (error) {

            })
    };
    
    this.registerUser = function (registerData) {

        console.log("register form data in controller: " + JSON.stringify(registerData));
        RegisterService.registerUser(registerData.username, registerData.email, registerData.dob, registerData.password, vm.profilePic).then(

            function (success) {

                console.log("success in register controller" + JSON.stringify(success));
                $state.go('login');

            },
            function (error) {

                console.log("success in register controller" + JSON.stringify(error));

            })

    };


}]);

registerModule.service('RegisterService', ['$q', 'CommonService', function ($q, CommonService) {

    var vm = this;

    this.registerUser = function (username, email, dob, password, profilepic) {

        console.log('username in service: ' + username);
        console.log('email in service: ' + email);
        console.log('dob in service: ' + dob);
        console.log('password in service: ' + password);
        console.log('profilepic in service: ' + profilepic);

        var dfd = $q.defer();

        var query = function (buttonIndex) {
            if (buttonIndex == "1") {
                db.transaction(function (tx) {
                    tx.executeSql("INSERT OR REPLACE INTO APP_USER_REGISTER (USER_ID, USERNAME, EMAIL, DOB, PASSWORD, PROF_PIC) VALUES (?,?,?,?,?,?)", [CommonService.getRandomNumber(), username, email, CommonService.formatDobToDb(dob), password, profilepic], function (tx, results) {

                        console.log("Data added successfully" + results);
                        navigator.notification.alert("Registered Successfully!!", null, "Register", "OK");
                        dfd.resolve("success");


                    }, function (tx, err) {

                        console.log("Error inserting data" + err.message);
                        dfd.resolve(null);

                    })
                })
            }
        }

        navigator.notification.confirm("Sure To Register?", query,'Register','Yes, No');

        return dfd.promise;
    };

    this.checkEmailExist = function (username) {

        var dfd = $q.defer();

        db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM APP_USER_REGISTER WHERE USERNAME = ?", [username], function (tx, res) {

                if (res.rows.length > 0) {

                    dfd.resolve("USER_EXIST");
                } else {

                    dfd.resolve("USER_NOT_EXIST");
                }

            }, function (tx, err) {

                console.log("Error in query" + err.message);
                dfd.resolve(null);

            })
        })

        return dfd.promise;
    };

}]);