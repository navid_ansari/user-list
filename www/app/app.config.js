app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    //	"use strict";
    //	if (IS_DEBUG)
    //    	debug = console.log.bind(window.console);
    //    else
    //    	debug = function(){};
    db = window.sqlitePlugin.openDatabase({
        name: "SA_DB_MAIN.db",
        location: 'default'
    });
    $stateProvider
        .state("login", {
            url: "/login",
            templateUrl: "./app/01login/login.html",
            controller: "LoginCtrl as LC"
        })
        .state("register", {
            url: "/register",
            templateUrl: "./app/02register/register.html",
            controller: "RegisterCtrl as RC"
        })
        .state("dashboard", {
            url: "/dashboard",
            templateUrl: "./app/03dashboard/dashboard.html",
            controller: "DashboardCtrl as DC",
            params: {
                USERDATA: null,
                CONSTANT: null
            },
            resolve: {
                USERDATA: ['$stateParams', function ($stateParams) {
                        //alert("userdata from login page" + JSON.stringify($stateParams.USERDATA));
                        return $stateParams.USERDATA;
                    }
                ],
                CONSTANT: ['$stateParams', function ($stateParams) {
                        //alert("constant from login page" + JSON.stringify($stateParams.CONSTANT));
                        return $stateParams.CONSTANT;
                    }
                ]
            }
        })
        .state("userform", {
            url: "/userform",
            templateUrl: "./app/04userform/userform.html",
            controller: "UserformCtrl as UC",
            abstract: true,
            cache: false,
        })
        .state("userform.addlead", {
            url: "/addlead",
            cache: false,
            params: {
                LEAD_ID: null,
                NEW_LEAD: null
            },
            views: {
                'fact-finder-content': {
                    templateUrl: "./app/04userform/01addlead/addlead.html",
                    controller: "AddleadCtrl as ALC",
                    resolve: {
                        StateData: ['AddleadService', function (AddleadService) {
                                //alert("state data");
                                return AddleadService.getStateData();
                    }
                ],
                        CityData: ['AddleadService', function (AddleadService) {
                                //alert("city data");
                                return AddleadService.getCityData();
                    }
                ],
                        ActiveTab: ['UserformService',
						function (UserformService) {
                                UserformService.setActiveTab("addLead");
                                //return "addLead";
						}
					],
                        LEAD_ID: ['$stateParams', function ($stateParams) {
                                //alert("lead id: " + $stateParams.LEAD_ID);
                                return $stateParams.LEAD_ID;
                            }
                        ],

                        NEW_LEAD: ['$stateParams', function ($stateParams) {
                                //alert("new lead: " + $stateParams.NEW_LEAD);
                                return $stateParams.NEW_LEAD;
                            }
                        ]
                    }


                }
            }

        })
        .state("userform.factfinder", {
            url: "/factfinder",
            params: { //always write params outside views
                LEAD_ID: null
            },
            views: {
                'fact-finder-content': {
                    templateUrl: "./app/04userform/02factfinder/factfinder.html",
                    controller: "FactfinderCtrl as FFC",
                    resolve: {
                        LEAD_ID: ['$stateParams', function ($stateParams) {
                                //alert("lead id: " + $stateParams.LEAD_ID);
                                return $stateParams.LEAD_ID;
                            }
                        ],
                        ActiveTab: ['UserformService',
						function (UserformService) {
                                UserformService.setActiveTab("factFinder");
                                //return "factFinder";
						}
					]
                    }
                }
            }

        })
        .state("userform.sis", {
            url: "/sis",
            params: { //always write params outside views
                LEAD_ID: null
            },
            views: {
                'fact-finder-content': {
                    templateUrl: "./app/04userform/03sis/sis.html",
                    controller: "sisCtrl as SISC",
                    resolve: {
                        LEAD_ID: ['$stateParams', function ($stateParams) {
                                //alert("lead id in sis: " + $stateParams.LEAD_ID);
                                return $stateParams.LEAD_ID;
                            }
                        ],
                        ActiveTab: ['UserformService',
						function (UserformService) {
                                UserformService.setActiveTab("sis");
                                //return "factFinder";
						}
					]
                    }
                }
            }

        })
        .state("userdashboard", {
            url: "/userdashboard",
            templateUrl: "./app/05userdashboard/userdashboard.html",
            controller: "userDashCtrl as UDC",
            abstract: true
        })
        .state("userdashboard.home", {
            url: "/home",
            views: {
                'user-dashboard-content': {
                    templateUrl: "./app/05userdashboard/home/home.html",
                    controller: "userHomeCtrl as UHC",

                }
            }

        });


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise("login");

}]);



//            params: {
//                USERID: null
//            },
//            resolve: {
//
//                USERID: ['$stateParams', 'factFinderService', function ($stateParams, factFinderService) {
//                    factFinderService.factUserID = $stateParams.USERID;
//                    return $stateParams.USERID;
//                                        }],
//
//                APP_MODULE_STATUS: ['factFinderService', function (factFinderService) {
//                    return factFinderService.appStatus(factFinderService.factUserID);
//                                        }],
//                
//               NomExistingData :['nomService','ApplicationFormDataService','LoginService',function(nomService,ApplicationFormDataService,LoginService){
//                 	  return nomService.existingData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
//                 						}],
//            }