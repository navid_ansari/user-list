var db = null;

var app = angular.module('app', ['ionic', 'ui.bootstrap', 'ngMessages','ngCordova','ui.select', 'app.common', 'app.01login', 'app.02register','app.03dashboard','app.04userform','app.05userdashboard'])

.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {

        //alert(JSON.stringify(ionic.Platform.device()));

        var deviceInformation = ionic.Platform.device();
        console.log("Device Information: "+JSON.stringify(deviceInformation));
        console.log('platform: ' + deviceInformation.platform);
        console.log('udid: ' + deviceInformation.uuid);

        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})