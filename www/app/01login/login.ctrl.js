loginModule.controller('LoginCtrl', ['$scope', '$state', '$log', 'LoginService', function ($scope, $state, $log, LoginService) {

    console.log("login controller loaded");

    this.goToRegister = function () {

        console.log("inside register function");
        $state.go('register');

    };
    
    this.goToUserDash = function(){
        
        $state.go('userdashboard.home');
    }

    this.loginUser = function () {

        console.log("login function running");
        console.log(JSON.stringify(this.login));

        LoginService.loginUser(this.login.username, this.login.password).then(
            function (success) {
                //$log.info("success in controller: " + JSON.stringify(success));

                if (success == 'USER_NOT_EXIST') {

                    alert("user do not exist");

                } else if (success == 'WRONG_PASSWORD') {

                    alert("wrong password");
                } else {

                    $log.info("user data in login success: " + JSON.stringify(success));
                    var constant = "test data";
                    $state.go("dashboard", {
                        USERDATA: success,
                        CONSTANT: constant
                    });
                    navigator.notification.alert("Logged in successfully!!", null, "Login", "OK");
                }
            },

            function (error) {
                console.log("success in controller: " + error);

            });
    }
}]);

loginModule.service('LoginService', ['$q', '$log', function ($q, $log) {
    var vm = this;

    this.loginUser = function (username, password) {

        var dfd = $q.defer();

        vm.loginObj = {};

        db.transaction(function (tx) {
            tx.executeSql("SELECT USER_ID, USERNAME, EMAIL, PASSWORD, PROF_PIC FROM APP_USER_REGISTER WHERE USERNAME = ?", [username], function (tx, res) {

                if (res.rows.length > 0) {

                    if (password == res.rows.item(0).PASSWORD) {

                        vm.loginObj.user_id = res.rows.item(0).USER_ID;
                        vm.loginObj.user_name = res.rows.item(0).USERNAME;
                        vm.loginObj.email_id = res.rows.item(0).EMAIL;
                        vm.loginObj.password = res.rows.item(0).PASSWORD;
                        vm.loginObj.prof_pic = res.rows.item(0).PROF_PIC;
                        $log.info("USER_EXIST: " + JSON.stringify(res));
                        dfd.resolve(vm.loginObj);
                    } else {

                        dfd.resolve('WRONG_PASSWORD');
                    }

                } else {

                    dfd.resolve('USER_NOT_EXIST');
                }

            }, function (tx, err) {

                console.log('USER_NOT_EXIST' + err.message);
                dfd.resolve("USER_NOT_EXIST");
            })
        })

        return dfd.promise;
    }

    //fetch data example

    this.fetchAllUserDetails = function () {
        //console.log("User detail called");
        var dfd = $q.defer();
        var dataArray = [];
        //var dataObject = {};

        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM App_User_Register', [], function (tx, results) {

                if (results.rows.length > 0) {
                    for (var i = 0; i < results.rows.length; i++) {
                        var dataObject = {};
                        dataObject.Username = results.rows.item(i).Username;
                        dataObject.Email = results.rows.item(i).Email;
                        dataObject.Mobile = results.rows.item(i).Mobile;
                        dataObject.Password = results.rows.item(i).Password;
                        console.log("dataObject" + JSON.stringify(dataObject));
                        dataArray.push(dataObject);
                    }
                    console.log("Database Value" + JSON.stringify(dataArray));
                    dfd.resolve(dataArray);
                    console.log("Data fetched");

                } else {

                    console.log("App_User_Register is empty");
                    dfd.resolve(null);

                }

            }, function (tx, err) {

                console.log("Error in  query" + err.message);
                dfd.resolve(null);

            })
        }, function (err) {

            console.log("Error in transaction " + err.message);
            dfd.resolve(null);

        }, null);

        return dfd.promise;
    };


}]);