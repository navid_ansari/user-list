dashboardModule.controller('DashboardCtrl', ['$state', '$log', 'USERDATA', 'CONSTANT', 'LoginService', 'DashboardService', 'AddleadService', 'persInfoService', '$scope', function ($state, $log, USERDATA, CONSTANT, LoginService, DashboardService, AddleadService, persInfoService, $scope) {

    console.log("dashboard controller running");
    var vm = this;
    
    //ui bootstrap acoordian
    $scope.oneAtATime = true;
    $scope.status = {
        isCustomHeaderOpen: false,
        isFirstOpen: true,
        isFirstDisabled: false
    };
    
    this.AccGroups = [
    {
      title: 'Dynamic Group Header - 1',
      content: 'Dynamic Group Body - 1'
    },
    {
      title: 'Dynamic Group Header - 2',
      content: 'Dynamic Group Body - 2'
    }
  ];
    
    this.accordionArray = [];

    //console.log(LoginService.loginObj.user_name);
    this.user_data = USERDATA;
    this.constant = CONSTANT;
    //console.log("username: " + this.user_data.user_name);


    if (USERDATA == undefined) {

        vm.user_data = {};
        console.log("dashboard getter: " + JSON.stringify(DashboardService.getDashboardData()));
        vm.DashboardFormData = DashboardService.getDashboardData();
        $log.info('Dashboard data: ' + JSON.stringify(vm.DashboardFormData));
        vm.user_data.prof_pic = vm.DashboardFormData.profilepic;
        vm.user_data.user_name = vm.DashboardFormData.username;
        vm.user_data.email_id = vm.DashboardFormData.email;
    }




    this.continueUserForm = function () {

        DashboardService.setDashboadrData(vm.user_data.prof_pic, vm.user_data.user_name, vm.user_data.email_id);
        $state.go('userform.addlead');

    }

    this.AddNewLead = function () {
        DashboardService.setDashboadrData(vm.user_data.prof_pic, vm.user_data.user_name, vm.user_data.email_id);

        $state.go('userform.addlead', {
            NEW_LEAD: 'NEW_LEAD_FLAG'
        });


        //DashboardService.continueUserForm();
    }

    if (AddleadService.leadDataObj.continue_user_flag == 'Y') {

        this.continueLead = true
    }

    this.tab = 1;

    this.setTab = function (tabId) {
        this.tab = tabId;
    };

    this.isSet = function (tabId) {
        return this.tab == tabId;
    };

}]);

dashboardModule.service('DashboardService', ['$state', '$log', 'persInfoService', function ($state, $log, persInfoService) {

    var vm = this;
    vm.DashboardDataObj = {};

    this.setDashboadrData = function (profilepic, username, email) {

        vm.DashboardDataObj.profilepic = profilepic;
        vm.DashboardDataObj.username = username;
        vm.DashboardDataObj.email = email;

    }

    this.getDashboardData = function () {

        return vm.DashboardDataObj;
    }



    this.gotoPage = function (pageName) {
        persInfoService.setActiveTab(pageName);
        if (pageName == 'personalDetails') {
            alert('personal details active');
        } else if (pageName == 'contactDetails') {
            alert('contact details active');
        } else {
            alert('address Details active');
        }
    }


}]);