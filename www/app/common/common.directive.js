commonModule.directive('validateDob', ['CommonService', function (CommonService) {
    "use strict";
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            function customValidator(ngModelValue) {
                console.log("ngModelValue: " + JSON.stringify(ngModelValue));
                if (!!ngModelValue) {
                    var newDateFormat = ngModelValue.substring(8, 10) + "-" + ngModelValue.substring(5, 7) + "-" + ngModelValue.substring(0, 4);
                    var date = newDateFormat + " 00:00:00";
                    if (CommonService.getAge(date) < ADULT_AGE)
                        ctrl.$setValidity('minAge', false);
                    else
                        ctrl.$setValidity('minAge', true);
                } else if (!!ngModelValue && ngModelValue === "") {
                    ctrl.$setValidity('minAge', true);
                }
                return ngModelValue;
            }
            ctrl.$parsers.unshift(customValidator); // when user manually changes the value
            ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
        }
    };
}]);

commonModule.directive('validateEmailRemotely', function ($http, $log, RegisterService) {
    return {
        restrict: 'A',
        scope: true,
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrls) {
            var ngModel = ctrls;
            var vm = this;
            scope.$watch(attrs.ngModel, function (username) {

                RegisterService.checkEmailExist(username).then(
                    function (response) {

                        $log.info("email exist" + JSON.stringify(response));

                        if (response == "USER_EXIST") {
                            console.log("ngModel validity false");
                            ctrls.$setValidity('validEmail', false);

                        } else {
                            ctrls.$setValidity('validEmail', true);
                        }
                    },
                    function (error) {

                        $log.info("check user error in controller" + error);

                    });
            });
        }
    }
});

commonModule.directive('myLink', function () {
    alert('working');
    return {
        scope: {
            enabled: '=myLink'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function (event) {
                if (!scope.enabled) {
                    event.preventDefault();
                }
            });

        }
    };
});