//regular expression for validations

    var USERNAME_REGEX = /^[a-zA-Z0-9]*$/;
	var NAME_REGEX = /^([a-zA-Z]){1}([a-zA-Z'-]*)$/;
	var MOBILE_REGEX = /^[7-9][0-9]{9}$/;
	var EMAIL_REGEX = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.(?:[A-Za-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/;

	var ZIPCODE_REGEX=/^(\d{5}-\d{4}|\d{6})$/;
	var FULLNAME_REGEX=/^([a-zA-Z]){1}([a-zA-Z ]*)$/;
	var FULLNAME_REGEX_SP=/^([a-zA-Z]){1}([a-zA-Z '-]*)$/;
	var MAND_REG = /\S/;
	var ALPHA_REG = /^[a-zA-Z]{1,150}$/;
	var NUM_REG = /^[0-9]*$/;
	var ADHAAR_REG = /^[0-9]{12}$/;
	var eIANo_REG = /^[0-9]{1,13}$/;
	var PAN_REG = /^([a-zA-Z]){3}([PCHFATBLJGpchfatbljg]){1}([a-zA-Z]){1}([0-9]){4}([a-zA-Z]){1}?$/;
	var VERNAC_WITNESS_NAME_REGEX = /^([a-zA-Z]){1}([a-zA-Z'-.\s]*)$/;
	var ALPHA_WITHSPACE_REG = /^[a-zA-Z\s]*$/;
	var ALPHA_NUM_WITHSPACE_REG = /^[a-zA-Z\s0-9]*$/;
	var DATE_REG = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
	var PINCODE_REG = /^[1-9][0-9]{5}$/;
	var BLOCKHASH = /^[^#|]*$/;
	var IFSC_REG = /^([A-Za-z]){4}([A-Za-z0-9]){7}$/;
	var AGENT_CD_REGEX = /^[1-9][0-9]*$/;

	var FATCA_ADD_REGEX = /^[a-zA-Z0-9 ,/().'-@]*$/;
	var FATCA_ADD_PIN = /^[a-zA-Z0-9 -]*$/;

	var FREE_TEXT_REGEX = /^[a-zA-Z0-9 ,/().'-]*$/;

    //adult age
    var ADULT_AGE = 18;