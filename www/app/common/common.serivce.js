commonModule.service("CommonService", ['$q', function ($q) {

    console.log("common service loaded");
    
    //calculate age
    this.getAge = function (birth) {
        var age;
        var today = new Date();
        var nowyear = today.getFullYear();
        var nowmonth = today.getMonth() + 1;
        var nowday = today.getDate();
        birth = birth.replace("-", "");
        birth = birth.replace("-", "");
        var birthday = birth.substring(0, 2); //6, 8
        var birthmonth = birth.substring(2, 4); //4, 6
        var birthyear = birth.substring(4, 8); //0, 4
        age = nowyear - birthyear;
        var age_month = nowmonth - birthmonth;
        var age_day = nowday - birthday;
        if (age_month < 0 || (age_month === 0 && age_day < 0)) {
            age = parseInt(age) - 1;
        }
        return age;
    };
    
    //generate random no for user id
    this.getRandomNumber = function() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getMilliseconds();
		var random = Math.floor((Math.random() * 100000) + 1000);
		random=''+random+h+m+s;
		return random;
	};
    
    //format utc date to dd-mm-yy 00:00:00
    this.formatDobToDb = function (ogDate){
		try{
			var date = new Date(ogDate); // so that below manipulation(s) doesn't affect parameter ogDate
			//date.setMonth(date.getMonth()+1);
			var _month = date.getMonth()+1;
			return ((date.getDate()>9)?date.getDate():("0"+date.getDate())) + "-" + ((_month>9)?(_month):("0"+_month)) + "-" + date.getFullYear() + " 00:00:00";
		}catch(ex){
			debug("Exception in formatDobToDb(): " + ex.message);
			return null;
		}
	};
    
    //format dd-mm-yy 00:00:00 to 
	this.formatDobFromDb = function (dob){
		return new Date(dob.substring(6,10)/* YYYY */ + "-" + dob.substring(3,5)/* MM */ + "-" + dob.substring(0,2)/* DD */);
	};
    
    //check internet connection on device
    this.checkConnection = function(){
		return navigator.onLine?"online":"offline"; //ternary operator, true returns online, false returns offline
	};
    
    //get current date
    this.getCurrDate = function(){
		try{
			var d = new Date();
			var dformat = [d.getDate().padLeft(),(d.getMonth()+1).padLeft(),d.getFullYear()].join('-') +' ' + [d.getHours().padLeft(),d.getMinutes().padLeft(), d.getSeconds().padLeft()].join(':');
			console.log("current date: "+dformat);
            return dformat;
		}catch(ex){
			debug("Exception in getCurrDate(): " + ex.message);
			return null;
		}
	};
    
    this.compairCurrentDateWithUTC = function(){
        var dfd = $q.defer();
        if(cs.checkConnection()== "online"){
	            var serverURL = "https://google.com"
	            var geturl;
	        geturl = $.ajax({
	            type: "GET",
	            url: serverURL,
	            success: function (res, status, xhr) {
	                var dateTime = xhr.getResponseHeader("date");
	                debug("dateTime : "+dateTime);
	               // dateTime = dateTime.replace("GMT","");
	               var compairResponse = {};

	                var utcTime =  cs.convertUTCToLocal(dateTime);
	                var convertedTime  =  cs.getDifferenceLocalToUTC(dateTime);
	                compairResponse.utcTime = utcTime;
	                compairResponse.convertedTime = convertedTime;
	                compairResponse.CurrDate = cs.getCurrDate();
	                dfd.resolve(compairResponse);
	            },
				error:function(xlt,ajaxResponse,errorThrown){
					debug("Error thrown is "+errorThrown);
					dfd.resolve(null);
				}
            });
        }else{
            dfd.resolve(null);
        }
        return dfd.promise;
     };
     this.getDifferenceLocalToUTC = function(utcDateTime){
             var localTime  = moment.utc(utcDateTime).toDate();
             localTime = moment(utcDateTime);
             debug("convertUTCToLocal dateTime : "+localTime);
             var deviceTime = moment(new Date());
             debug("deviceTime is : "+deviceTime);
             var duration = moment.duration(deviceTime.diff(localTime));
             debug("duration is : "+duration);
             var hours = duration.asHours();
             debug("hours are "+hours);
             var checkIfValidate = (hours > 2 || hours < -2 )?false:true;
             debug("round of the float "+Math.round(hours));
             return checkIfValidate;

    };

	this.getCurrDateUTC = function(){
    	    var dfd = $q.defer();
    	    if(cs.checkConnection()== "online"){
				 var serverURL = "https://google.com"
				 var geturl;
				  geturl = $.ajax({
					type: "GET",
					url: serverURL,
					success: function (res, status, xhr) {
						var dateTime = xhr.getResponseHeader("date");
						debug("dateTime : "+dateTime);
					   // dateTime = dateTime.replace("GMT","");
						var convertedTime  =  cs.convertUTCToLocal(dateTime);
						dfd.resolve(convertedTime);
					}
				  });
              }else{
              		dfd.resolve(null);
              }
              return dfd.promise;
    	};
    
	this.convertUTCToLocal = function(utcDateTime){
		var localTime  = moment.utc(utcDateTime).toDate();
	   // localTime = moment(utcDateTime).format('EEE, dd MMM yyyy HH:mm:ss Z');
		//localTime = moment(utcDateTime).format("llll")
		localTime = moment(utcDateTime).format("DD-MM-YYYY HH:mm:ss");
		debug("convertUTCToLocal dateTime : "+localTime);
	//	cs.setCurrDateTime(localTime);
		return localTime;
	};
//	this.getCurrDateTime = function(){
//		return cs.currDateTime;
//	};
//
//	this.setCurrDateTime = function(dateTime){
//    	 cs.currDateTime = dateTime;
//    };

	this.getFiscalYearData = function(){
		try{
			//get current date
			var today = new Date();
			//get current month
			var curMonth = today.getMonth();
			var fiscalYr1 = "";
			var fiscalYr2 = "";
			var fiscalYr3 = "";
			var last3YearArr = [];
			if (curMonth > 3) {
				var currYr = (today.getFullYear()).toString();
				var nextYr1 = (today.getFullYear() + 1).toString();
				var prevYr1 = (today.getFullYear() - 1).toString();
				var prevYr2 = (today.getFullYear() - 2).toString();
				fiscalYr1 = today.getFullYear().toString() + "-" + nextYr1.charAt(2) + nextYr1.charAt(3);
				fiscalYr2 = prevYr1 + "-" + currYr.charAt(2) + currYr.charAt(3);
				fiscalYr3 = prevYr2 + "-" + prevYr1.charAt(2) + prevYr1.charAt(3);
				last3YearArr=[fiscalYr1,fiscalYr2,fiscalYr3];
				return last3YearArr;
			} else {
				var currYr = (today.getFullYear()).toString();
				var prevYr1 = (today.getFullYear() - 1).toString();
				var prevYr2 = (today.getFullYear() - 2).toString();
				var prevYr3 = (today.getFullYear() - 3).toString();
				fiscalYr1 = prevYr1 + "-" + currYr.charAt(2) + currYr.charAt(3);
				fiscalYr2 = prevYr2 + "-" + prevYr1.charAt(2) + prevYr1.charAt(3);
				fiscalYr3 = prevYr3 + "-" + prevYr2.charAt(2) + prevYr2.charAt(3);
				last3YearArr=[fiscalYr1,fiscalYr2,fiscalYr3];
				return last3YearArr;
			}
		}catch(ex){
			debug("Exception in getFiscalYearData(): " + ex.message);
			return null;
		}
	};

	this.get6MonthYearData = function(){
		try {
			var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			var today = new Date();
			//today.setMonth(today.getMonth() + 2);
			var d;
			var month;
			var initMonth = today.getMonth()+1;
			var monthYearData = [];

			for(var i = 1; i <= 6; i++) {
				var obj = {};
				d = new Date(today.getFullYear(), today.getMonth() - i, 1);
				month = monthNames[d.getMonth()];
				obj.MONTH = month;
				if(initMonth < 6 && d.getMonth()+1>6){
					obj.YEAR = today.getFullYear()-1;
				}
				else{
					obj.YEAR = today.getFullYear();
				}
				monthYearData.push(obj);
			}
			return monthYearData;
		} catch (e) {
			debug("Exception in get6MonthYearData(): " + ex.message);
			return null;
		}

	};
    this.getNoOfMonths = function (dateToCopmare){
		var Nomonths;
		var date2 = new Date();
		dateToCopmare=dateToCopmare.replace("-", "");
		dateToCopmare=dateToCopmare.replace("-", "");
		var day=dateToCopmare.substring(0,2); //6, 8
		var month=dateToCopmare.substring(2,4); //4, 6
		var year=dateToCopmare.substring(4,8); //0, 4
		var date1 = new Date(year, month, day);
		Nomonths= (date2.getFullYear() - date1.getFullYear()) * 12;
		Nomonths-= date1.getMonth() + 1;
		Nomonths+= date2.getMonth() +1; // we should add + 1 to get correct month number
		return Nomonths <= 0 ? 0 : Nomonths;
	};
    
    this.doSign = function(action){
		var opts = {
			animation: true,
			templateUrl: 'signature/signature.html',
			controller: "SignatureCtrl as sc",
			resolve: {
				Action: [
					function(){
						debug("resolving action: " + action);
						return action;
					}
				]
			}
		};
		this.modalInstance = $uibModal.open(opts);
	};
    
    this.toTitleCase = function(str){
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	};
    
    this.ekycServletCall = function(aadharNumber, timestamp, otp){
		var dfd = $q.defer();

		var data = {"Type": "o", "AadhaarNo": aadharNumber};

		if(!!timestamp){
			data.Type = "b";
			data.TimeStamp = timestamp.substring(6,10)/* YYYY */ + "-" + timestamp.substring(3,5)/* MM */ + "-" + timestamp.substring(0,2)/* DD */ + " " + timestamp.substring(11,20);
			data.OTP = otp;
		}

		debug("data:: " + JSON.stringify(data));

		$http({
			url: EKYC_SERVLET,
			method: AJAX_TYPE,
			headers: {'Content-Type': TYPE_JSON},
			params: data,
			async: AJAX_ASYNC || true,
			timeout: 1000*60*(AJAX_TIMEOUT || 1)
		}).success(function(response){
			debug(response);
			dfd.resolve(response);
		}).error(function(data, status, headers, config, statusText){
			navigator.notification.alert("Could not process the request right now. Please try again later.",function(){
				cs.hideLoading();
			},"Server Request","OK");
			debug("Error in ajax request: " + data);
			debug("Error status: " + status + " - " + statusText);
			dfd.resolve(null);
		});

		return dfd.promise;
	};

}]);