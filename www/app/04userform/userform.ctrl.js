userFormModule.controller('UserformCtrl', ['$state', '$log', '$rootScope', '$scope', 'UserformService', 'AddleadService','persInfoService', function ($state, $log, $rootScope, $scope, UserformService, AddleadService,persInfoService) {

    console.log("user form controller running");

    //$rootScope.progressWidth = 0;
    var vm = this;
    $log.info('abstract state running');
    this.factDisabled = true;

    $scope.$on('progress-bar', function (event, data) {

        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("$broadcast value: " + JSON.stringify(data));
        vm.progressWidth = data;

    });

    $scope.$on('fact-finder-state', function (event, data) {
        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("$broadcast value: " + JSON.stringify(data));
        vm.factDisabled = data;

    });

    //fact timeline disabled
    this.timelineFactFinder = true;
    $scope.$on('timeline-factfinder', function (event, data) {
        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("fact finder disabled: " + JSON.stringify(data));
        vm.timelineFactFinder = data;

    });

    //sis timeline disabled
    this.timelineSIS = true;
    $scope.$on('timeline-sis', function (event, data) {
        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("sis disabled: " + JSON.stringify(data));
        vm.timelineSIS = data;

    });

    this.addleadClass = false;
    $scope.$on('add-lead-class', function (event, data) {
        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("$broadcast:add-lead-class-> " + JSON.stringify(data));
        vm.addleadClass = data;

    });


    this.factfinderClass = false;
    $scope.$on('fact-finder-class', function (event, data) {
        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("$broadcast:fact-finder-class-> " + JSON.stringify(data));
        vm.factfinderClass = data;

    });

    this.sisClass = false;
    $scope.$on('sis-class', function (event, data) {
        //console.log("$broadcast value: "+JSON.stringify(event));
        console.log("$broadcast:sis-class-> " + JSON.stringify(data));
        vm.sisClass = data;

    });


    this.changeState = function (statename) {

        UserformService.changeState(statename);
    }

    this.isTabActive = function (tabname) {

        return ((!!tabname) && (!!UserformService.getActiveTab()) && (tabname == UserformService.getActiveTab())) ? true : false
    }

    //    this.timelineActiveTab = function(timelinetab){
    //        
    //        UserformService.timelineActiveTab(timelinetab);
    //        
    //    }

    this.timelineActiveTab = function (statename) {
        console.log('statename for timeline: ' + statename);

        if (statename == 'addlead') {

            $state.go('userform.addlead', {
                LEAD_ID: AddleadService.leadDataObj.leadid
            });

        } else if (statename == 'factfinder') {

            $state.go('userform.factfinder', {
                LEAD_ID: AddleadService.leadDataObj.leadid
            });

        } else if (statename == 'sis') {

            $state.go('userform.sis', {
                LEAD_ID: AddleadService.leadDataObj.leadid
            });
        }
    }

    this.isTimelineActive = function (timelineActiveClass) {

        console.log((!!UserformService.getTimelineTab()) && (timelineActiveClass == UserformService.getTimelineTab()));
        console.log((UserformService.getTimelineTab()));
        console.log(timelineActiveClass);
        return ((!!UserformService.getTimelineTab()) && (timelineActiveClass == UserformService.getTimelineTab())) ? true : false
    }
    
    //persInfoService.setPillActiveTab("personalDetails");
    
    
    
    
}]);

userFormModule.service('UserformService', ['$state', '$log','persInfoService', function ($state, $log, persInfoService) {

    console.log("user form service running");

    var vm = this;

    this.activeTab;

    this.timelineTab;

    this.setActiveTab = function (activeTab) {
        this.activeTab = activeTab;
        console.log("active tab: " + this.activeTab);
    };

    this.getActiveTab = function () {

        return this.activeTab
    }

    this.setTimelineTab = function (timelinetab) {

        this.timelineTab = timelinetab;
        console.log("active timeline: " + this.timelineTab);

    }

    this.getTimelineTab = function () {

        return this.timelineTab;
    }

    this.changeState = function (statename) {

        if (statename == 'addLead') {

            if (this.getActiveTab() !== statename) {

                $state.go('userform.addlead');
            }
        } else if (statename == 'factFinder') {

            if (this.getActiveTab() !== statename) {

                $state.go('userform.factfinder');
            }
        }
    }
    
    this.gotoPage = function(pagename){
        
        persInfoService.setPillActiveTab(pagename);
        
        $log.info('page name: '+pagename);
        
        if(pagename == 'personalDetails'){
            
            $state.go('userform.addlead');
        }
        else if(pagename == 'contactDetails'){
            
            $state.go('userform.factfinder');
        }
        else{
            
            $state.go('userform.sis');
            
        }
    }





}]);