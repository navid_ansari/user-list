addLead.controller('AddleadCtrl', ['$scope', '$log', '$state', 'CommonService', 'StateData', 'CityData', 'AddleadService', '$filter', 'LEAD_ID', '$rootScope', 'FactfinderService', 'NEW_LEAD','persInfoService', function ($scope, $log, $state, CommonService, StateData, CityData, AddleadService, $filter, LEAD_ID, $rootScope, FactfinderService, NEW_LEAD,persInfoService) {

    console.log("Add lead controller running");
    console.log("NEW LEAD in controller: " + NEW_LEAD);
    //    if(CommonService.checkConnection == "offline"){
    //        
    //        navigator.notification.alert("Mobile Data is on offline mode!!", null, "Mobile Data Status", 'OK');
    //    }
    var vm = this;
    
    persInfoService.setPillActiveTab("personalDetails");
    
    this.newLeadFlag = NEW_LEAD;
    this.backToDashboard = function () {

        $state.go('dashboard');

    }

    this.usernameRegex = USERNAME_REGEX;
    this.nameRegex = NAME_REGEX;
    this.mobileRegex = MOBILE_REGEX;
    this.emailRegex = EMAIL_REGEX;

    vm.stateData = StateData;
    vm.cityData = CityData;
    //    vm.cityByStateCode;
    //    $log.info('city data global: '+JSON.stringify(vm.cityByStateCode));
    vm.lead = {}

    //$log.info("state data in controller: " + JSON.stringify(this.stateData));
    //$log.info("city data in controller: " + JSON.stringify(this.cityData));

    this.getCities = function (stateCode) {

        this.statecode = stateCode.state_code;

        console.log("state code in controller: " + JSON.stringify(stateCode));


        AddleadService.getCities(this.statecode).then(

            function (success) {

                //$log.info("success in city data state code function: " + JSON.stringify(success));
                vm.cityByStateCode = success;
                $log.info('city data from database: ' + JSON.stringify(vm.cityByStateCode));
                vm.lead.city = vm.cityByStateCode[0].state_desc;
            },
            function (error) {

                console.log("error in city data state code function: " + error);

            })
    }

    this.preferredContactList = [{
        LOV_CD: 1,
        LOV_VALUE: 'MOBILE'
    }, {
        LOV_CD: 2,
        LOV_VALUE: 'LANDLINE'
    }, {
        LOV_CD: 3,
        LOV_VALUE: 'ALTERNATE'
    }];

    this.lead.prefcontact = this.preferredContactList[0].LOV_VALUE;

    console.log("LeadData :" + JSON.stringify(AddleadService.getLeadData()));

    vm.leadFormData = AddleadService.getLeadData(); //get stored object data

    //    if (!!this.newLeadFlag) {
    //        $log.info('checking lead flag');
    //        AddleadService.leadDataObj = null;
    //        FactfinderService.factFinderBeanObj = null;
    //
    //    }

    if (this.newLeadFlag == 'NEW_LEAD_FLAG') {
        console.log('check new lead flag');
        
        //AddleadService.leadDataObj = {};// cont user flag cannot be set null or empty so empty each entry explicitly except cont user flag
        AddleadService.leadDataObj.leadid = '';
        AddleadService.leadDataObj.fName = '';
        AddleadService.leadDataObj.lName = '';
        AddleadService.leadDataObj.dob = '';
        AddleadService.leadDataObj.gender = '';
        AddleadService.leadDataObj.mobile = '';
        AddleadService.leadDataObj.landline = '';
        AddleadService.leadDataObj.alternate = '';
        AddleadService.leadDataObj.prefcontact = '';
        AddleadService.leadDataObj.state = '';
        AddleadService.leadDataObj.city = '';
        
        FactfinderService.factFinderBeanObj = {};
    }

    console.log("leadFormData :" + JSON.stringify(vm.leadFormData));

    this.lead.fname = vm.leadFormData.fName;
    this.lead.lname = vm.leadFormData.lName;
    this.lead.dob = vm.leadFormData.dob;
    this.lead.gender = vm.leadFormData.gender;
    this.lead.mobile = vm.leadFormData.mobile;
    this.lead.landline = vm.leadFormData.landline;
    this.lead.alternate = vm.leadFormData.alternate;
    this.lead.prefcontact = vm.leadFormData.prefcontact;

    //$log.info('dob from object: '+vm.leadFormData.dob);
    //$log.info('leadid from object: '+vm.leadFormData.leadid);
    $log.info('lead id in lead controller' + LEAD_ID);

    vm.continue_lead_flag = 'Y';

    this.addLead = function () {

        var landlineNo = this.lead.landline != '' ? this.lead.landline : '';
        var alternateNo = this.lead.alternate != '' ? this.lead.alternate : '';

        //            console.log("fname in controller :" + this.lead.fname);
        //            console.log("lname in controller :" + this.lead.lname);
        //            console.log("dob in controller :" + this.lead.dob);
        //            console.log("gender in controller :" + this.lead.gender);
        //            console.log("gender in controller :" + JSON.stringify(this.lead.gender));
        //            console.log("mobileno in controller :" + this.lead.mobile);
        //            console.log("landlineno in controller :" + landlineNo);
        //            console.log("alternateno in controller :" + alternateNo);
        //            console.log("prefcontact in controller :" + JSON.stringify(this.lead.prefcontact));
        //            console.log("prefcontact in controller :" + this.lead.prefcontact.LOV_VALUE);
        //            console.log("state in controller :" + JSON.stringify(this.lead.state));
        //            console.log("state in controller :" + this.lead.state.state_desc);
        //            console.log("city in controller :" + JSON.stringify(this.lead.city));
        //            console.log("city in controller :" + this.lead.city.state_desc);

        if (LEAD_ID == undefined || LEAD_ID == null) {

            this.LeadId = CommonService.getRandomNumber();

            //empty the bean data of all the pages
            //AddleadService.leadDataObj = null;
            //FactfinderService.factFinderBeanObj = null;

            console.log("lead id in add lead controller :" + this.LeadId);


            AddleadService.setLeadData(this.LeadId, this.lead.fname, this.lead.lname, this.lead.dob, this.lead.gender, this.lead.mobile, landlineNo, alternateNo, this.lead.prefcontact.LOV_VALUE, this.lead.state.state_desc, this.lead.city.state_desc, vm.continue_lead_flag); // set data in object   

            AddleadService.addLead(this.LeadId, this.lead.fname, this.lead.lname, CommonService.formatDobToDb(this.lead.dob), this.lead.gender, this.lead.mobile, landlineNo, alternateNo, this.lead.prefcontact.LOV_VALUE, this.lead.state.state_desc, this.lead.city.state_desc).then(



                function (success) {

                    //AddleadService.leadDataObj = '';
                    //FactfinderService.factFinderBeanObj = '';

                    if (success == "success") {
                        console.log("success: Lead added: ");
                        console.log("lead id before state.go: " + vm.LeadId);
                        navigator.notification.alert("Lead added successfully!!", null, "Add Lead", "OK");
                        $state.go("userform.factfinder", {
                            LEAD_ID: vm.LeadId
                        });
                    }

                },
                function (error) {

                    console.log("error: lead not added" + error);

                })

        } else {

            AddleadService.setLeadData(LEAD_ID, this.lead.fname, this.lead.lname, this.lead.dob, this.lead.gender, this.lead.mobile, landlineNo, alternateNo, this.lead.prefcontact.LOV_VALUE, this.lead.state.state_desc, this.lead.city.state_desc, vm.continue_lead_flag); // set data in object   

            AddleadService.updateLead(LEAD_ID, this.lead.fname, this.lead.lname, CommonService.formatDobToDb(this.lead.dob), this.lead.gender, this.lead.mobile, landlineNo, alternateNo, this.lead.prefcontact.LOV_VALUE, this.lead.state.state_desc, this.lead.city.state_desc).then(

                function (success) {

                    if (success == "success") {

                        console.log("success: Lead added: ");
                        console.log("lead id before state.go: " + LEAD_ID);
                        navigator.notification.alert("Lead updated successfully!!", null, "Add Lead", "OK");
                        $state.go("userform.factfinder", {
                            LEAD_ID: LEAD_ID
                        });
                    }

                },
                function (error) {

                    console.log("error: lead not added" + error);

                })

        }


    }

    if (!!vm.lead.prefcontact) {

        vm.lead.prefcontact = $filter('filter')(vm.preferredContactList, {
            "LOV_VALUE": vm.leadFormData.prefcontact
        })[0];
    }

    if (!!vm.leadFormData.state) {

        vm.lead.state = $filter('filter')(vm.stateData, {
            "state_desc": vm.leadFormData.state
        })[0];
    }

    if (!!vm.leadFormData.city) {

        //$log.info('tempCity: ' + JSON.stringify(vm.tempCity));
        //vm.lead.city = $filter('filter')(vm.tempCity, {"state_desc": vm.leadFormData.city})[0];
    }

}]);

addLead.service('AddleadService', ['$q', 'CommonService', function ($q, CommonService) {

    var vm = this;


    vm.leadDataObj = {};

    vm.setLeadData = function (leadid, fName, lName, dob, gender, mobile, landline, alternate, prefcontact, state, city, cont_lead_flag) {
        console.log('lead id in add lead set data: ' + leadid);

        vm.leadDataObj.leadid = leadid;
        vm.leadDataObj.fName = fName;
        vm.leadDataObj.lName = lName;
        vm.leadDataObj.dob = dob;
        vm.leadDataObj.gender = gender;
        vm.leadDataObj.mobile = mobile;
        vm.leadDataObj.landline = landline;
        vm.leadDataObj.alternate = alternate;
        vm.leadDataObj.prefcontact = prefcontact;
        vm.leadDataObj.state = state;
        vm.leadDataObj.city = city;
        vm.leadDataObj.continue_user_flag = cont_lead_flag;
    };

    vm.getLeadData = function () {

        return vm.leadDataObj;
    };


    this.jsonData = {
        name: 'navid',
        email: 'navid@gmail.com'
    };

    this.getStateData = function () {

        var dfd = $q.defer();

        db.transaction(function (tx) {
            //SELECT STATE_CODE, STATE_DESC, ISACTIVE FROM LP_STATE_MASTER
            tx.executeSql("SELECT STATE_CODE, STATE_DESC, ISACTIVE FROM LP_STATE_MASTER", [], function (tx, res) {

                if (!!res && res.rows.length > 0) {

                    var stateArray = [];

                    for (var i = 0; i < res.rows.length; i++) {
                        var stateObj = {};
                        stateObj.state_code = res.rows.item(i).STATE_CODE;
                        stateObj.state_desc = res.rows.item(i).STATE_DESC;
                        stateObj.is_active = res.rows.item(i).ISACTIVE;
                        stateArray.push(stateObj);
                    }
                    dfd.resolve(stateArray);
                } else {

                    dfd.resolve(null);
                }
            }, function (tx, err) {

                console.log('error in state query' + err.message);

            })
        })

        return dfd.promise;
    };

    this.getCityData = function () {

        var dfd = $q.defer();

        db.transaction(function (tx) {
            tx.executeSql("SELECT CITY_CODE, CITY_DESC, STATE_CODE FROM LP_CITY_MASTER", [], function (tx, res) {

                if (!!res && res.rows.length > 0) {

                    var cityArray = [];

                    for (var i = 0; i < res.rows.length; i++) {
                        var cityObj = {};
                        cityObj.city_code = res.rows.item(i).CITY_CODE;
                        cityObj.city_desc = res.rows.item(i).CITY_DESC;
                        cityObj.state_code = res.rows.item(i).STATE_CODE;
                        cityArray.push(cityObj);
                    }
                    dfd.resolve(cityArray);

                } else {

                    dfd.resolve(null);
                }
            }, function (tx, err) {

                console.log('error in city query' + err.message);
            })
        })

        return dfd.promise;
    };

    this.getCities = function (statecode) {

        console.log("city data by state code in service: " + JSON.stringify(statecode));

        var dfd = $q.defer();

        db.transaction(function (tx) {
            //SELECT STATE_CODE, STATE_DESC, ISACTIVE FROM LP_STATE_MASTER
            tx.executeSql("SELECT CITY_CODE, CITY_DESC, STATE_CODE FROM LP_CITY_MASTER WHERE STATE_CODE = ?", [statecode], function (tx, res) {

                if (!!res && res.rows.length > 0) {
                    var city_code_Array = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        var city_code_Obj = {};
                        city_code_Obj.city_code = res.rows.item(i).CITY_CODE;
                        city_code_Obj.state_desc = res.rows.item(i).CITY_DESC;
                        city_code_Obj.state_code = res.rows.item(i).STATE_CODE;
                        city_code_Array.push(city_code_Obj);
                    }
                    console.log("success in city code query");
                    dfd.resolve(city_code_Array);

                } else {

                    dfd.resolve(null);
                }
            }, function (tx, err) {

                console.log('error in city code query' + err.message);

            })
        })

        return dfd.promise;
    };

    this.addLead = function (leadid, fname, lname, dob, gender, mobileno, landlineno, alternateno, prefcontact, state, city) {

        //        console.log("lead id in service: " + leadid);
        //        console.log("fname in service: " + fname);
        //        console.log("lname in service: " + lname);
        //        console.log("dob in service: " + dob);
        //        console.log("gender in service: " + gender);
        //        console.log("mobileno in service: " + mobileno);
        //        console.log("landlineno in service: " + landlineno);
        //        console.log("alternateno in service: " + alternateno);
        //        console.log("prefcontact in service: " + prefcontact);
        //        console.log("state in service: " + state);
        //        console.log("city in service: " + city);
        var dfd = $q.defer();



        db.transaction(function (tx) {
            tx.executeSql("INSERT OR REPLACE INTO APP_ADD_LEAD(LEAD_ID, LEAD_FNAME, LEAD_LNAME, LEAD_DOB, LEAD_GENDER, LEAD_MOBILE, LEAD_LANDLINE, LEAD_ALTERNATE, LEAD_PREF, LEAD_STATE, LEAD_CITY) VALUES (?,?,?,?,?,?,?,?,?,?,?)", [leadid, fname, lname, CommonService.formatDobToDb(dob), gender, mobileno, landlineno, alternateno, prefcontact, state, city], function (tx, res) {



                db.transaction(function (tx) {
                    tx.executeSql("INSERT OR REPLACE INTO APP_FORM_STATUS(LEAD_ID, LEAD) VALUES (?,?)", [leadid, "Y", ], function (tx, res) {

                        //navigator.notification.alert("Lead added successfully!!", null, "Add Lead", "OK");
                        //dfd.resolve("success");
                        console.log("APP_FORM_STATUS table updated: ");
                        dfd.resolve("success");

                    }, function (tx, err) {

                        console.log('error in APP_FORM_STATUS query' + err.message);
                    })
                })


            }, function (tx, err) {

                console.log('error in add lead query' + err.message);
            })
        })

        return dfd.promise;

    }

    this.updateLead = function (leadid, fname, lname, dob, gender, mobileno, landlineno, alternateno, prefcontact, state, city) {

        console.log("update lead id in service: " + leadid);
        console.log("update fname in service: " + fname);
        console.log("update lname in service: " + lname);
        console.log("update dob in service: " + dob);
        console.log("update gender in service: " + gender);
        console.log("update mobileno in service: " + mobileno);
        console.log("update landlineno in service: " + landlineno);
        console.log("update alternateno in service: " + alternateno);
        console.log("update prefcontact in service: " + prefcontact);
        console.log("update state in service: " + state);
        console.log("update city in service: " + city);
        var dfd = $q.defer();

        db.transaction(function (tx) {
            tx.executeSql('UPDATE APP_ADD_LEAD SET LEAD_FNAME=?, LEAD_LNAME=?, LEAD_DOB=?, LEAD_GENDER=?, LEAD_MOBILE=?, LEAD_LANDLINE=?, LEAD_ALTERNATE=?, LEAD_PREF=?, LEAD_STATE=?, LEAD_CITY=? WHERE LEAD_ID=?', [fname, lname, CommonService.formatDobToDb(dob), gender, mobileno, landlineno, alternateno, prefcontact, state, city, leadid], function (tx, res) {
                dfd.resolve("success");

            }, function (tx, err) {

                console.log('error in add lead query' + err.message);
            })
        })

        return dfd.promise;

    }

}]);