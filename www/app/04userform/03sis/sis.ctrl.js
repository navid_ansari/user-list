sisModule.controller('sisCtrl', ['$scope','sisService','$rootScope','$state','LEAD_ID','persInfoService', function($scope,sisService,$rootScope,$state,LEAD_ID,persInfoService){
    
    console.log('sis controller running');
    var vm = this;
    
    persInfoService.setPillActiveTab("addressDetails");
    
    this.timelineSIS = false;
    $rootScope.$broadcast('timeline-sis', this.timelineSIS);
    
    this.factfinderClass = true;
    $rootScope.$broadcast('fact-finder-class', this.factfinderClass);
    
    this.GoBackToFactFinder = function(){
        
        $state.go('userform.factfinder',{LEAD_ID:LEAD_ID});
    }
    
}]);

sisModule.service('sisService', ['$state', function($state){
    
    console.log('sis service running');
    
}]);