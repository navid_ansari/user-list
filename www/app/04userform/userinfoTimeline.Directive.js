userFormModule.controller('persInfoCtrl', ['persInfoService','$log','UserformService', function (persInfoService, $log, UserformService) {

    this.perSideTabArr = [
        {
            'type': 'personalDetails',
            'name': 'Personal',
            'state':'userform.addlead'
        },
        {
            'type': 'contactDetails',
            'name': 'Contact',
            'state':'userform.factfinder'
        },
        {
            'type': 'addressDetails',
            'name': 'Address',
            'state':'userform.sis'
        },
    ]

    this.PillActiveTab = persInfoService.getPillActiveTab();
    
    $log.info('active tab set: '+persInfoService.getPillActiveTab());
//    $log.info('active tab set');
    

    this.gotoPage = function (pageName) {
        UserformService.gotoPage(pageName);
    };

    
}]);

userFormModule.service('persInfoService',['$state','$log', function($state,$log){
    //debug("personal service loaded");

    this.currentActiveTab;

    this.setPillActiveTab = function (activeTab) {
        $log.info('active tab name in setter: '+activeTab);        
        this.currentActiveTab = activeTab;
    };
    
    
    this.getPillActiveTab = function () {

        return this.currentActiveTab;
    }
    
    
    
}]);


userFormModule.directive('piTimeline', [function () {
    //"use strict";
    //debug('in piTimeline');
    return {
        restrict: 'E',
        controller: "persInfoCtrl",
        controllerAs: "pstc",
        bindToController: true,
        templateUrl: "app/04userform/userinfoTimeline.html"
    };
}]);