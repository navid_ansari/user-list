factfinderModule.controller('FactfinderCtrl', ['$scope', 'AddleadService', 'LEAD_ID', 'FactfinderService', '$rootScope', '$state', '$log', '$filter','persInfoService', function ($scope, AddleadService, LEAD_ID, FactfinderService, $rootScope, $state, $log, $filter,persInfoService) {

    console.log("fact finder controller running");
    $log.info("lead id in fact finder controller: " + LEAD_ID);
    console.log("data from service: " + JSON.stringify(AddleadService.jsonData));

    var vm = this;
    
    persInfoService.setPillActiveTab("contactDetails");
    
    vm.progressWidth = 50;

    $rootScope.$broadcast('progress-bar', vm.progressWidth); // update progressWidth value in userform.ctrl.js

    this.factDisabled = false;
    $rootScope.$broadcast('fact-finder-state', this.factDisabled);

    //timeline: fact finder link disabled
    this.timelineFactFinder = false;
    $rootScope.$broadcast('timeline-factfinder', this.timelineFactFinder);

    //timeline: add lead ng-class
    this.addleadClass = true;
    $rootScope.$broadcast('add-lead-class', this.addleadClass);

    vm.total;
    vm.factfinder = {};

    vm.equityOption = [{
        choice: 'Equity 90% and Debt 10 %'
    }, {
        choice: 'Equity 60% and Debt 40 %'
    }, {
        choice: 'Equity 30% and Debt 70 %'
    }, {
        choice: 'Equity 0% and Debt 100 %'
    }];

    //this.factfinder.payOutOpt = 'Lumpsum Payout at Maturity';

    this.equityChange = function (factfinderForm) {

        console.log("equity change running: " + this.factfinder.equityFund);
        console.log(this.factfinder.equityFund != undefined);

        var equity = this.factfinder.equityFund != undefined ? this.factfinder.equityFund : 0;
        var balance = this.factfinder.balancedFund != undefined ? this.factfinder.balancedFund : 0;
        var debt = this.factfinder.debtFund != undefined ? this.factfinder.debtFund : 0;

        console.log("equity: " + equity);
        console.log("balance: " + balance);
        console.log("debt: " + debt);

        if ((parseInt(this.factfinder.equityFund) <= 100) || (parseInt(this.factfinder.balancedFund) <= 100) || (parseInt(this.factfinder.debtFund) <= 100)) {

            vm.total = parseInt(equity) + parseInt(balance) + parseInt(debt);
            if (this.factfinder.equityFund != undefined && this.factfinder.balancedFund != undefined && this.factfinder.debtFund) {

                if (vm.total < 100 || vm.total > 100) {

                    navigator.notification.alert("Total should be equal to 100!", null, "Fund Value", "OK");
                    //factfinderForm.EquityFund.$setValidity('max', false);
                    vm.factfinder.equityFund = null;
                    vm.factfinder.balancedFund = null;
                    vm.factfinder.debtFund = null;
                    vm.total = '';
                }
            }

            //            if (vm.total > 100) {
            //
            //                navigator.notification.alert("Total must be equal to 100!", null, "Fund Value", "OK");
            //                //factfinderForm.Total.$setValidity('max', false);
            //                vm.factfinder.equityFund = null;
            //                vm.factfinder.balancedFund = null;
            //                vm.factfinder.debtFund = null;
            //                vm.total = '';
            //            }
        }
    }

    vm.factFinderBeanData = FactfinderService.getFactFinderFormData();
    $log.info('fact finder bean data: ' + JSON.stringify(vm.factFinderBeanData));

    //this.factFinder.leadid =  vm.factFinderBeanData.leadid;
    //console.log('lead id in bean controller: ' + this.factfinder.leadid);

    //console.log('payout: ' + vm.factFinderBeanData.payout);
    

    this.factfinder.payOutOpt = vm.factFinderBeanData.payout;
    
    if (!!vm.factFinderBeanData.invest) {

        this.factfinder.invest = $filter('filter')(vm.equityOption, {
            "choice": vm.factFinderBeanData.invest
        })[0];
    }

    this.factfinder.investing = vm.factFinderBeanData.investing;
    this.factfinder.equityFund = vm.factFinderBeanData.equity;
    this.factfinder.balancedFund = vm.factFinderBeanData.balance;
    this.factfinder.debtFund = vm.factFinderBeanData.debt;

    this.addFactFinder = function () {

        console.log("pref payout: " + this.factfinder.payOutOpt);
        console.log("invest: " + this.factfinder.invest.choice);
        console.log("investing: " + this.factfinder.investing);
        console.log("equityFund: " + this.factfinder.equityFund);
        console.log("balancedFund: " + this.factfinder.balancedFund);
        console.log("debtFund: " + this.factfinder.debtFund);
        console.log("total: " + this.total);

        var equity = this.factfinder.equityFund != '' ? this.factfinder.equityFund : '';
        var balance = this.factfinder.balancedFund != '' ? this.factfinder.balancedFund : '';
        var debt = this.factfinder.debtFund != '' ? this.factfinder.debtFund : '';
        var totalValue = this.total != '' ? this.total : '';

        console.log("equity: " + equity);
        console.log("balance: " + balance);
        console.log("debt: " + debt);
        console.log("totalValue: " + totalValue);
        FactfinderService.setFactFinderFormData(LEAD_ID, this.factfinder.payOutOpt, this.factfinder.invest.choice, this.factfinder.investing, equity, balance, debt);

        FactfinderService.addFactFinder(LEAD_ID, this.factfinder.payOutOpt, this.factfinder.invest.choice, this.factfinder.investing, equity, balance, debt, totalValue).then(

            function (success) {

                console.log("success in fact finder controller");
                if (success == 'fact_finder_added') {

                    navigator.notification.alert("Fact Finder added successfully!!", null, "Fact Finder", "OK");
                    $state.go('userform.sis', {
                        LEAD_ID: LEAD_ID
                    });
                } else {

                    navigator.notification.alert("Fact Finder updated successfully!!", null, "Fact Finder", "OK");
                    $state.go('userform.sis', {
                        LEAD_ID: LEAD_ID
                    });
                }

            },
            function (error) {

                console.log("error in fact finder controller");
            })

    }

    this.GoBackToLead = function () {

        $state.go('userform.addlead', {
            LEAD_ID: LEAD_ID
        });
    }

}]);

factfinderModule.service('FactfinderService', ['$q', '$log', function ($q, $log) {

    console.log("fact finder service running");
    var vm = this;
    
    vm.factFinderBeanObj = {};

    this.setFactFinderFormData = function (leadid, payout, invest, investing, equity, balance, debt, total) {

        vm.factFinderBeanObj.leadid = leadid;
        vm.factFinderBeanObj.payout = payout;
        vm.factFinderBeanObj.invest = invest;
        vm.factFinderBeanObj.investing = investing;
        vm.factFinderBeanObj.equity = equity;
        vm.factFinderBeanObj.balance = balance;
        vm.factFinderBeanObj.debt = debt;
        vm.factFinderBeanObj.total = total;

    };

    this.getFactFinderFormData = function () {

        return vm.factFinderBeanObj;
    }

    this.addFactFinder = function (leadid, payout, invest, investing, equity, balance, debt, totalvalue) {

        console.log("lead id in service: " + leadid);
        console.log("pref payout in service: " + payout);
        console.log("invest in service: " + invest);
        console.log("investing in service: " + investing);
        console.log("equityFund in service: " + equity);
        console.log("balancedFund in service: " + balance);
        console.log("debtFund in service: " + debt);
        console.log("debtFund in service: " + totalvalue);
        var dfd = $q.defer();

        db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM APP_FACT_FINDER WHERE LEAD_ID=?", [leadid], function (tx, res) {

                if (res.rows.length > 0) {
                    $log.info('lead id exist in fact finder table: updating..')
                    db.transaction(function (tx) {
                        tx.executeSql("UPDATE APP_FACT_FINDER SET FACT_PAYOUT=?, FACT_INVEST=?, FACT_INVESTING=?, FACT_EQUITY=?, FACT_BALANCE=?, FACT_DEBT=?, FACT_TOTAL=? WHERE LEAD_ID=?", [payout, invest, investing, equity, balance, debt, totalvalue, leadid], function (tx, res) {

                            dfd.resolve('fact_finder_updated');

                        }, function (tx, err) {

                            console.log('error in add lead query' + err.message);
                        })
                    })

                } else {

                    db.transaction(function (tx) {
                        tx.executeSql("INSERT OR REPLACE INTO APP_FACT_FINDER(LEAD_ID, FACT_PAYOUT, FACT_INVEST, FACT_INVESTING, FACT_EQUITY, FACT_BALANCE, FACT_DEBT, FACT_TOTAL) VALUES (?,?,?,?,?,?,?,?)", [leadid, payout, invest, investing, equity, balance, debt, totalvalue], function (tx, res) {

                            db.transaction(function (tx) {
                                tx.executeSql("INSERT OR REPLACE INTO APP_FORM_STATUS(LEAD_ID, FACT_FINDER) VALUES (?,?)", [leadid, "Y", ], function (tx, res) {
                                    console.log("APP_FORM_STATUS of fact finder 'Y' updated: ");
                                    //                        navigator.notification.alert("Fact Finder added successfully!!", null, "Fact Finder", "OK");
                                    dfd.resolve("fact_finder_added");

                                }, function (tx, err) {

                                    console.log('error in APP_FORM_STATUS query' + err.message);
                                })
                            })

                        }, function (tx, err) {

                            console.log('error in add lead query' + err.message);
                        })
                    })
                }

            }, function (tx, err) {

                console.log('error in add lead query' + err.message);
            })
        })

        return dfd.promise;

    }

}]);